package co.simplon.alt6;

import java.io.File;

public class Navigation {

  // Method to navigate back to the parent folder (if possible)
  public boolean navigateBack(ExplorerState state) {
    String currentLocation = state.getCurrentLocation();
    File currentDirectory = new File(currentLocation);
    String parentPath = currentDirectory.getParent();

    if (parentPath != null) {
      state.setCurrentLocation(parentPath);
      return true; // Navigation successful
    } else {
      System.out.println("Already at the root directory. Cannot navigate back.");
      return false; // Navigation failed
    }
  }

  // Method to navigate into a specified folder (if it exists)
  public boolean navigateInto(ExplorerState state, String folder) {
    if (folder != null && !folder.isEmpty()) {
      String currentLocation = state.getCurrentLocation();
      String newPath = currentLocation + File.separator + folder;
      File newDirectory = new File(newPath);

      if (newDirectory.exists() && newDirectory.isDirectory()) {
        state.setCurrentLocation(newPath);
        return true; // Navigation successful
      } else {
        System.out.println("Invalid folder or folder does not exist: " + folder);
      }
    }
    return false; // Navigation failed
  }
}