package co.simplon.alt6;

import java.io.File;

public class FolderAction {

  public void createFolder(String location, String name) {
  
    File folder = new File(name);

    if (!folder.exists()) {
      boolean created = folder.mkdir();
      if (created) {
        System.out.println("Le dossier '" + name + "' a été créé avec succès.");
      } else {
        System.err.println("Impossible de créer le dossier '" + name + "'.");
      }
    } else {
      System.err.println("Le dossier '" + name + "' existe déjà.");
    }
  }
}
