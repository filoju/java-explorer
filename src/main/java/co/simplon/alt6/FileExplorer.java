package co.simplon.alt6;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

public class FileExplorer extends Application {

  @Override
  public void start(Stage primaryStage) {
    primaryStage.setTitle("File Explorer");
    ExplorerState explorerState = new ExplorerState();
    FolderAction folderAction = new FolderAction();
    Navigation navigation = new Navigation();
    FileAction fileAction = new FileAction();

    VBox buttonContainer = new VBox(10);
    ScrollPane scrollPane = new ScrollPane(buttonContainer);
    scrollPane.setFitToWidth(true);

    refreshButtons(explorerState, buttonContainer, navigation);

    // create folder button
    Button createFolderButton = new Button("Create folder");
    createFolderButton.setStyle("-fx-background-color: plum");

    createFolderButton.setOnAction(e -> {
      TextInputDialog folderNameField = new TextInputDialog();
      folderNameField.setHeaderText("Enter folder name:");
      folderNameField.setContentText("Folder Name:");

      folderNameField.showAndWait().ifPresent(folderName -> {
        if (!folderName.isEmpty()) {
          folderAction.createFolder(explorerState.getCurrentLocation(), folderName);
          refreshButtons(explorerState, buttonContainer, navigation);
        }
      });
    });

    // create file button
    Button createFileButton = new Button("Create file");
    createFileButton.setStyle("-fx-background-color: plum");

    createFileButton.setOnAction((e -> {
      TextInputDialog fileNameField = new TextInputDialog();
      fileNameField.setHeaderText("Enter file name:");
      fileNameField.setContentText("File name:");

      fileNameField.showAndWait().ifPresent(fileName -> {
        if (!fileName.isEmpty()) {
          fileAction.createFile(explorerState.getCurrentLocation(), fileName);
          refreshButtons(explorerState, buttonContainer, navigation);
        }
      });
    }));

    // back button
    Button previousButton = new Button("Previous");
    previousButton.setStyle("-fx-background-color: lemonchiffon");
    previousButton.setOnAction(e -> {
      boolean success = navigation.navigateBack(explorerState);
      if (success) {
        refreshButtons(explorerState, buttonContainer, navigation); // refresh buttons after going back
      }
    });

    // display
    HBox topButtons = new HBox(10);
    topButtons.getChildren().addAll(createFolderButton, createFileButton, previousButton);
    BorderPane layout = new BorderPane();
    // layout.setCenter(scrollPane);
    layout.setTop(topButtons);
    layout.setCenter(scrollPane);

    Scene scene = new Scene(layout, 600, 400);
    primaryStage.setScene(scene);
    primaryStage.show();
  }

  // content refresh
  private void refreshButtons(ExplorerState explorerState, VBox buttonContainer, Navigation navigation) {
    buttonContainer.getChildren().clear();
    List<String> folders = explorerState.listFolders();
    List<String> files = explorerState.listFiles();

    // folders
    for (String folder : folders) {
      Button folderButton = new Button(folder);
      folderButton.setStyle("-fx-background-color: lightpink");
      folderButton.setOnAction(e -> {
        boolean success = navigation.navigateInto(explorerState, folder);
        if (success) {
          refreshButtons(explorerState, buttonContainer, navigation); // refresh buttons after navigating into a folder
        }
      });
      buttonContainer.getChildren().add(folderButton);
    }

    // files
    for (String file : files) {
      Button fileButton = new Button(file);
      fileButton.setStyle("-fx-background-color: paleturquoise");
      fileButton.setOnAction(event -> {
        Stage fileStage = new Stage();
        fileStage.setTitle(file);

        TextArea textArea = new TextArea();
        textArea.setEditable(true);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
          String line;
          while ((line = br.readLine()) != null) {
            textArea.appendText(line + "\n");
          }
        } catch (IOException e) {
          e.printStackTrace();
        }

        // save button
        Button saveButton = new Button("Save");
        saveButton.setOnAction(saveEvent -> {
          String newContent = textArea.getText();
          FileAction fileAction = new FileAction();
          fileAction.modifyFile(file, newContent);
        });

        // delete button
        Button deleteButton = new Button("Delete");
        deleteButton.setOnAction(deleteEvent -> {
          Alert confirmDialog = new Alert(AlertType.CONFIRMATION);
          confirmDialog.setTitle("Confirmation");
          confirmDialog.setHeaderText("Delete File");
          confirmDialog.setContentText("Are you sure you want to delete this file?");

          ButtonType confirmButton = new ButtonType("OK");
          ButtonType cancelButton = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
          confirmDialog.getButtonTypes().setAll(confirmButton, cancelButton);

          Optional<ButtonType> result = confirmDialog.showAndWait();

          if (result.isPresent() && result.get() == confirmButton) {
            FileAction fileAction = new FileAction();
            fileAction.deleteFile(file);
            fileStage.close();
            refreshButtons(explorerState, buttonContainer, navigation);
          }
        });

        // save area
        VBox fileContent = new VBox(10);
        fileContent.getChildren().addAll(textArea, saveButton, deleteButton);

        Scene fileScene = new Scene(fileContent, 400, 400);
        fileStage.setScene(fileScene);
        fileStage.show();
      });
      buttonContainer.getChildren().add(fileButton);
    }
  }
}