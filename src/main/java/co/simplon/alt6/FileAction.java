package co.simplon.alt6;

import java.nio.file.Files;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileAction {

  public boolean createFile(String location, String fileName) {

    String filePath = location + File.separator + fileName;

    Path path = Paths.get(filePath);

    try {
      Files.createFile(path);
      System.out.println("File created successfully: " + path);
      return true;
    } catch (IOException e) {
      System.err.println("Failed to create the file: " + e.getMessage());
      return false;
    }

  }

  public void openFile(String fileName) {
    try {
      FileReader fileReader = new FileReader(fileName);
      BufferedReader bufferedReader = new BufferedReader(fileReader);
      String line;
      while ((line = bufferedReader.readLine()) != null) {
        System.out.println(line);
      }

      bufferedReader.close();
      fileReader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public void modifyFile(String fileName, String newContent) {
    try {
      FileWriter fileWriter = new FileWriter(fileName, false);
      BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
      bufferedWriter.write(newContent);
      bufferedWriter.newLine();
      bufferedWriter.close();
      fileWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void deleteFile(String fileName) {
    File fileToDelete = new File(fileName);
    if (fileToDelete.exists()) {
      if (fileToDelete.delete()) {
        System.out.println("File deleted successfully: " + fileName);
      } else {
        System.err.println("Failed to delete the file: " + fileName);
      }
    } else {
      System.err.println("File not found: " + fileName);
    }
  }
}
