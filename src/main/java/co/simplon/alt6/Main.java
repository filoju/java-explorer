package co.simplon.alt6;

import javafx.application.Application;

public class Main {
  public static void main(String[] args) {

    // Launch the JavaFX application (FileExplorer)
    Application.launch(FileExplorer.class, args);

  }
}
