package co.simplon.alt6;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ExplorerState {
  private String currentLocation;

  // Constructor to initialize currentLocation to the current working directory
  public ExplorerState() {
    this.currentLocation = System.getProperty("user.dir"); // si on veut que ça ouvre sur la racine = "/"
  }

  // Method to get the current location
  public String getCurrentLocation() {
    return currentLocation;
  }

  // Set the current location
  public void setCurrentLocation(String location) {
    this.currentLocation = location;
  }

  // List the folders and files in the current location
  public List<String> listContent() {

    List<String> content = new ArrayList<>();
    File currentDirectory = new File(currentLocation);

    if (currentDirectory.exists() && currentDirectory.isDirectory()) {
      File[] directoryContents = currentDirectory.listFiles();

      if (directoryContents != null) {
        for (File item : directoryContents) {
          content.add(item.getName());
        }
      }
    }
    return content;
  }

  public List<String> listFolders() {
    List<String> folders = new ArrayList<>();
    File currentDirectory = new File(currentLocation);

    if (currentDirectory.exists() && currentDirectory.isDirectory()) {
      File[] directoryContents = currentDirectory.listFiles();

      if (directoryContents != null) {
        for (File item : directoryContents) {
          if (item.isDirectory()) {
            folders.add(item.getName());
          }
        }
      }
    }
    return folders;
  }

  public List<String> listFiles() {
    List<String> files = new ArrayList<>();
    File currentDirectory = new File(currentLocation);

    if (currentDirectory.exists() && currentDirectory.isDirectory()) {
      File[] directoryContents = currentDirectory.listFiles();

      if (directoryContents != null) {
        for (File item : directoryContents) {
          if (item.isFile()) {
            files.add(item.getName());
          }
        }
      }
    }
    return files;
  }

}
