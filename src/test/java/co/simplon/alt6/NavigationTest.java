package co.simplon.alt6;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class NavigationTest {
  @Test
  void testNavigateBackTrue() {
    ExplorerState state = new ExplorerState();
    Navigation navigation = new Navigation();
    boolean result = navigation.navigateInto(state, "src");
    assertTrue(result);

  }

  @Test
  void testNavigateIntoFalse() {
    ExplorerState state = new ExplorerState();
    Navigation navigation = new Navigation();
    boolean result = navigation.navigateInto(state, "doesntexist");
    assertFalse(result);
  }
}
