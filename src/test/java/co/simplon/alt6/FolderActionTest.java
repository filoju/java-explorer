package co.simplon.alt6;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import static org.junit.jupiter.api.Assertions.assertTrue;
import java.io.File;
import java.util.List;

public class FolderActionTest {
    ExplorerState state = new ExplorerState();
    FolderAction folderAction = new FolderAction();

    @AfterEach
    void cleanUp() {
        String location = ""; // You should specify the directory path here
        String folderName = "NouveauDossier";
        File folder = new File(location, folderName);

        if (folder.exists()) {
            folder.delete();
        }
    }

    @Test
    void testCreateFolderSuccess() {
    ExplorerState explorerState = new ExplorerState();
    String folderName = "NouveauDossier";
    folderAction.createFolder(explorerState.getCurrentLocation(), folderName);
    List<String> folders = explorerState.listFolders();
    
    // Check if the newly created folder is in the list of folders
    assertTrue(folders.contains(folderName));
}


@Test
void testCreateFolderAlreadyExists() {
    ExplorerState explorerState = new ExplorerState();
    String folderName = "NouveauDossier";
    String location = explorerState.getCurrentLocation();

    // Create the folder manually before calling the createFolder method
    File folder = new File(location, folderName);
    folder.mkdirs();

    // Try to create the folder again using your folderAction
    folderAction.createFolder(location, folderName);

    // Get the list of folders after attempting to create the folder again
    List<String> folders = explorerState.listFolders();

    // Check if the folder still exists in the list of folders
    assertTrue(folders.contains(folderName));
}


}
